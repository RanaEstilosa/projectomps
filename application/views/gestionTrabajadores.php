<section id="main-content">
    <section class="wrapper"> 

	<?php
	
	?>
		<table id="trabajadores" class="display">
			<thead>
				<tr>
					<th>Nombre</th>
					<th>Apellido</th>
					<th>Nombre de usuario</th>
					<th>Area</th>
					<th>Editar</th>
				</tr>
			</thead>
			<tbody>

				<?php
				foreach ($trabajadores as $trabajador) {
				
				?>
				<tr id="rowTrabajador<?= $trabajador->ID_Trabajador?>">
					<td> <?= $trabajador->Nombre?> </td>
					<td> <?= $trabajador->Apellido?> </td>
					<td> <?= $trabajador->Username?> </td>
					<td> <?= $trabajador->Area?> </td>
					<td> <button type="button" id="trabajador-<?= $trabajador->ID_Trabajador ?>"class="eliminar btn btn-danger"><i class="fa fa-trash-o"></i></button> </td>
				</tr>

				<?php
				}
				?>

			</tbody>
		</table>
	</section>
</section> 

<script type="text/javascript">
// desde la vista llamaremos al controlador para eliminar
	$(".eliminar").click(function(){

		var IDTrabajador=this.id;
		var res=ID_Trabajador = IDTrabajador.split("-");
		var ID_Trabajador = res[1];
		console.log(ID_Trabajador);

		$.post("<?= base_url() ?>Dashboard/eliminarTrabajador", { IDTrabajador: ID_Trabajador }).done(function(data){
			$("#rowTrabajador"+ID_Trabajador).fadeOut(); // hace que se oculte
		});

	});
		


</script>

<script>
	$(document).ready( function () {
    	$('#trabajadores').DataTable({
			columnDefs: [{
				targets: [ 0 ],
				ordenData: [ 0, 1 ]
			},{
				targets: [ 1 ],
				ordenData: [ 1 , 0 ]
			},{
	 		targets: [ 4 ],
				ordenData: [ 4, 0 ]
			} ]
		} );
} );
</script>
