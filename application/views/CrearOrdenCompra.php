<header>
	<link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/css/WizzardOrdenDeCompra.css">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</header>
<section id="main-content">
    <section class="wrapper"> 
		<div class="container">
			<div class="stepwizard">
				<div class="stepwizard-row setup-panel">
					<div class="stepwizard-step col-xs-3"> 
						<a href="#step-1" type="button" class="btn btn-success btn-circle">1</a>
						<p><small>Pedido</small></p>
					</div>
					<div class="stepwizard-step col-xs-3"> 
						<a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
						<p><small>Producto</small></p>
					</div>
					<div class="stepwizard-step col-xs-3"> 
						<a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
						<p><small>Detalle producto</small></p>
					</div>
					<div class="stepwizard-step col-xs-3"> 
						<a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
						<p><small>Resumen</small></p>
					</div>
				</div>
			</div>
			<form role="form">
				<div class="panel panel-primary setup-content" id="step-1">
					<div class="panel-heading">
						<h3 class="panel-title">Pedido</h3>
					</div>
					<div class="panel-body">
						<div class="form-group col-sm-2">
						<label class="control-label">Folio :</label>
							<?php 
							foreach ($series as $serie){
							?>				
								<input class="form-control" id="Folio" name="folio"   value="<?= $serie->Folio+1 ?>"readonly>
							<?php
							}
							?>
							</input>
						</div>
						<div class="form-group col-sm-2">
							<label class="control-label">Rut Cliente :</label>
							<input type="search" name="rut" list="ListaRut" id="Rut" oninput="obtenerID()" class="form-control"  placeholder="Seleccione Rut" required>  
							<datalist id="ListaRut">  
								<?php 
									foreach($clientes as $cliente){		
								?>	
									<option   data-IDCliente="<?= $cliente -> ID_Cliente?>"  value="<?= $cliente -> Rut?>" > </option> 
										<?php
											}
										?>
							</datalist>
						</div>
						<div class="form-group col-sm-2">
							<label class="control-label">ID_Cliente :</label>
							<input type="text" required="required" id="IDCliente" class="form-control" name="idCliente" readonly>
						</div>
						<div class="form-group col-sm-2">
							<label class="control-label">Fecha Ingreso :</label>
								<input type="date"  name="fechaIngreso" id="FechaIngreso" class="form-control round-form" readonly value="<?php echo date("Y-m-d");?>">
						</div>
						<div class="form-group col-sm-2">
							<label class="control-label">Fecha Entrega :</label>
							<input type="date" required="required" name="fechaEntrega" id="FechaEntrega" class="form-control round-form" oninput="ValidarFecha()" required>
						</div>
					</div>
					<div class="panel-body">
						<div class="form-group col-sm-2">
							<label class="control-label">Tipo Trabajo :</label>
							<select type="text" id="TipoTrabajo" name="tipoTrabajo" class="form-control" placeholder="TipoTrabajo" required>
								<option value="">Elige una opción</option>
								<option scope="">Arreglo</option>
								<option scope="">Fabricacion</option>
								<option scope="">Falla</option>
							</select>
						</div>
						<div class="form-group col-sm-2">
							<label class="control-label">Pedido MercadoLibre :</label>
							<select type="text" id="PedidoExpress" name="pedidoExpress" class="form-control" placeholder="PedidoExpress"required >
								<option value="0">No</option>
								<option value="1">Si</option>
							</select>
						</div>
						<div class="form-group col-sm-2">
						</div>
						<div class="form-group col-sm-2">
							<label class="control-label">Ingresado Por</label>
							<input type="text" class="form-control" name="ingresadoPor" id="IngresadoPor" readonly value="<?php echo strtoupper( $_SESSION['Nombre']." ".$_SESSION['Apellido']);?>">

						</div>
					</div>	
					<div class="panel-body">
						<div class="form-group col-sm-2">
							<label class="control-label">Estado Pedido :</label>
							<select type="text" id="EstadoPedido" name="estadopedido" class="form-control" placeholder="EstadoPedido">
								<option value="">Seleccione una opcion</option>
								<option value="Espera Cliente">En espera del cliente</option>
								<option scope="Confirmado">Confirmado</option></option>
								<option scope="Corte">Corte</option>
								<option scope="Sublimacion">Sublimación</option>
								<option scope="Estampado">Estampado</option>
								<option scope="planchado">planchado</option>
								<option scope="empaquetado">empaquetado</option>
								<option scope="Compra">Compra</option>
								<option scope="Espera de retiro">Espera de retiro</option>
								<option scope="Despacho">Despacho</option>
								<option scope="Finalizado">Finalizado</option>
							</select>
						</div>	
						<div class="form-group col-sm-8">
							<button class="btn btn-primary nextBtn pull-right"  type="button" id="InsertarDatosCliente" onclick="InsertarPedido1()">Next</button>
						</div>
					</div>
				</div>
				
				<div class="panel panel-primary setup-content" id="step-2">
					<div class="panel-heading">
						<h3 class="panel-title">Producto</h3>
					</div>
					<div class="panel-body">
						<div class="col-sm-3">
							<label class="control-label">Producto :</label>
							<select class="form-control" id="Producto" name="producto" required oninput="obtenerIDProducto()"> 
							<option value="">Seleccione una opcion</option>
								<?php 
								foreach ($productos as $producto){
								?>				
									<option data-IDProducto="<?= $producto->ID_TipoProducto?>" value="<?= $producto->Nombre?>"><?= $producto->Nombre?></option>
								<?php
								}
								?>
							</select>
						</div>
						<div class="form-group col-sm-2" style="display: none;" >
							<label class="control-label">ID_Producto :</label>
							<input type="text" required="required" name ="idtipoproducto" id="IDTipoProducto" class="form-control" name="idProducto" readonly>
						</div>
						<div class="col-sm-2">
							<label class="control-label">Tipo Manga :</label>
							<select class="form-control" id="TipoManga" name="tipomanga"> 
							<option value="0">Seleccione una opcion</option>
							<?php
								foreach($tipomangas as $tipomanga){
								?>
								<option value="<?= $tipomanga->TipoManga?>"><?= $tipomanga->TipoManga?> </option>
								<?php
								}
								?>
							</select>
						</div>
						<div class="col-sm-2">
							<label class="control-label">Tipo Cuello :</label>
							<select class="form-control" id="TipoCuello" name="tipocuello"> 
							<option value="0">Seleccione una opcion</option>
							<?php
								foreach($tipocuellos as $tipocuello){
								?>
								<option value="<?= $tipocuello->TipoCuello?>"><?= $tipocuello->TipoCuello?> </option>
								<?php
								}
								?>
							</select>
						</div>
						<div class="col-sm-2">
							<label class="control-label">Cantidad :</label>
							<input type="text" required="required" name="productocantidad" class="form-control" id="ProductoCantidad" required>
						</div>
						<div class="col-sm-2">
							<label class="control-label">Estilo Equipo :</label>
							<select class="form-control" id="EstiloEquipo" name="estiloEquipo"> 
							<option value="0">Seleccione una opcion</option>
							<?php
								foreach($estiloequipos as $estiloequipo){
								?>
								<option value="<?= $estiloequipo->Nombre?>"><?= $estiloequipo->Nombre?> </option>
								<?php
								}
								?>
							</select>
						</div>
					</div>
					<div class="panel-body">
						<div class="form-group col-sm-2">
							<label class="control-label">Sublimado :</label>
							<select class="form-control" id="EsSublimado" name="essublimado" >
								<option value="No">No</option>
								<option value="Si">Si</option>	
							</select>		
						</div>
						<div class="col-sm-2">
							<label class="control-label">Color 1:</label>
							<input type="search" name="color1" list="ListaColor" id="Color1" class="form-control"  placeholder="Seleccione Color">  
								<datalist id="ListaColor">  
									<option   value="" ></option> 
									<?php 
										foreach($colores as $color){		
									?>	
										<option   value="<?= $color->Color?>" ><?= $color->Color?> </option> 
											<?php
									}
										?>
								</datalist>
						</div>	
						<div class="col-sm-2">
						<label class="control-label">Color 2:</label>
							<input type="search" name="color2" list="ListaColor" id="Color2" class="form-control"  placeholder="Seleccione Color">  
								<datalist id="ListaColor">  
									<option   value="" ></option> 
									<?php 
										foreach($colores as $color){		
									?>	
										<option   value="<?= $color->Color?>" ><?= $color->Color?> </option> 
											<?php
									}
										?>
								</datalist>
						</div>	
						<div class="col-sm-2">
						<label class="control-label">Color 3:</label>
							<input type="search" name="color3" list="ListaColor" id="Color3" class="form-control"  placeholder="Seleccione Color">  
								<datalist id="ListaColor">  
									<option value="" ></option> 
									<?php 
										foreach($colores as $color){		
									?>	
										<option   value="<?= $color->Color?>" ><?= $color->Color?> </option> 
											<?php
									}
										?>
								</datalist>
						</div>	
						<div class="col-sm-4">
						<label class="control-label">Imagen Adjunta :</label>
							<div class="col-sm-10">
							<input class="form-control"  type="file" name="archivo">
							</div>
						</div>
					</div>
					<div class="panel-body">
					<button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
					</div>	
				</div>
				
				<div class="panel panel-primary setup-content" id="step-3">
					<div class="panel-heading">
							<h3 class="panel-title" id="DetalleProducto">  </h3>
					</div>		
						<div align="right" style="margin-bottom:5px;">
							<button type="button" name="Agregar" id="Agregar" class="btn btn-success btn-circle"><i class="fa fa-plus"></i></button>
						</div>
						<form method="post" id="user_form">
							<div class="table-responsive">
								<table class="table table-striped table-bordered" id="user_data">
									<tr>
										<th>Cantidad</th>
										<th>Genero</th>
										<th>Talla</th>
										<th>ID_Producto</th>
										<th>Accion</th>
									</tr>
								</table>
							</div>
							<div align="center">
								<input type="submit" name="registrar" id="Registrar" class="btn btn-primary" value="Registrar" />
							</div>
						</form>
					<div id="DetalleProductoLista" title=#Agregar>
						<div class="form-group">
							<label>Ingrese Cantidad</label>
							<input type="number" name="cantidad" id="Cantidad" class="form-control" />
							<span id="error_cantidad" class="number-danger"></span>
						</div>
						<div class="form-group">
							<label class="control-label">Genero :</label>
							<select class="form-control" id="Genero" name="genero" > 
								<option value="0">Seleccione una opcion</option>
								<?php
								foreach($generos as $genero){
								?>
								<option value="<?= $genero->Genero?>"><?= $genero->Genero?> </option>
								<?php
								}
								?>			
							</select>
							<span id="error_genero" class="text-danger"></span>
						</div>
						<div class="form-group">
							<label class="control-label">Talla :</label>
							<select class="form-control" id="Talla" name="talla" > 
								<option value="0">Seleccione una opcion</option>
								<?php
								foreach($tallas as $talla){
								?>
								<option value="<?= $talla->Talla?>"><?= $talla->Talla?> </option>
								<?php
								}
								?>
							</select>
						</div>
						<div class="form-group">
							<label class="control-label">ID Producto :</label>
							<select class="form-control" id="IDProductoFinal" name="idproductofinal" readonly> 
								<?php
									foreach($idproductos as $idproducto){
								?>
									<option value="<?= $idproducto->ID_Producto?>"><?= $idproducto->ID_Producto?> </option>
								<?php
								}
								?>
							</select>
						</div>
						<div class="form-group" align="center">
							<input type="hidden" name="row_id" id="hidden_row_id" />
							<button type="button" name="guardar" id="GuardarDetalleProducto" class="btn btn-info">Guardar</button>
						</div>
					</div>
				</div>
				<div class="panel-body">
					<button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
				</div>
				
				
				<div class="panel panel-primary setup-content" id="step-4">
					<div class="panel-heading">
						<h3 class="panel-title">Cargo</h3>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<label class="control-label">Company Name</label>
							<input maxlength="200" type="text" required="required" class="form-control" placeholder="Enter Company Name" />
						</div>
						<div class="form-group">
							<label class="control-label">Company Address</label>
							<input maxlength="200" type="text" required="required" class="form-control" placeholder="Enter Company Address" />
						</div>
						<button class="btn btn-success pull-right" type="submit">Finish!</button>
					</div>
				</div>
			</form>
		</div>
	</section>
</section>

<script> // funciones
 	function  obtenerID() {
		// obtenermos el id que esta guardado en data-idcliente
		var rutCliente = $('#Rut').val();
		var ID_Cliente = $('#ListaRut [value="'+rutCliente+'"]').data('idcliente');
		// en otro imnput guardamos el ID
		$("#IDCliente").val(ID_Cliente);
	 }
	 
	 function ValidarFecha() {
		var fechainvalida = "dd/mm/aaaa"
		var fechaingreso = $('#FechaIngreso').val();
		var fechaentrega = $('#FechaEntrega').val();
		var IDCliente = $("#IDCliente").val();
		if (fechaentrega <fechaingreso){
			alert("Fecha Ingresada no valida");
			$('#FechaEntrega').val(fechainvalida);
		}
	}

	function  obtenerIDProducto() {
	// obtenermos el id que esta guardado en data-IDTipoProducto
	var producto = $('#Producto').val()
	var ID_Producto = $('#Producto [value="'+producto+'"]').data('idproducto');
	// en otro imnput guardamos el ID
	$("#IDTipoProducto").val(ID_Producto);
	 }
//Eventos Click 
	$('#Producto').change(function(){
		obtenerIDProducto();
		CargaTipoMangas();
		CargaTipoCuellos();
		CargarGenero();
		document.getElementById("DetalleProducto").innerHTML = document.getElementById("ProductoCantidad").value +" "+$('#Producto').val();		
	});

	$('#Genero').change(function(){
		CargarTallas();
	});
	$('#Talla').change(function(){
		CargarIDProductoFinal();
	});

	$('#TipoManga').change(function(){
		CargaTipoCuellos();
		CargarGeneros();
		CargarTallas();
		document.getElementById("DetalleProducto").innerHTML = $('#Producto').val()+" "+$('#TipoCuello').val()	
	});

	$('#TipoCuello').change(function(){
		CargarGeneros();
		CargarTallas();
		document.getElementById("DetalleProducto").innerHTML = $('#Producto').val()+" "+$('#TipoManga').val()+" "+$('#TipoCuello').val();
	});

	$('#ProductoCantidad').change(function(){
		document.getElementById("DetalleProducto").innerHTML = $('#ProductoCantidad').val()+" "+$('#Producto').val()+" "+$('#TipoManga').val()+" "+$('#TipoCuello').val();
	});
	$('#InsertarDatosCliente').change(function(){
		InsertarPedido1();
	});
	

// JS ajax
	function CargarGeneros(){
		$.ajax({
			type:"POST",
			url:"ObtenerProductoGenero",
			data:"tipocuello=" + $('#TipoCuello').val(),
			success:function(r){
				$('#Genero').html(r);
			}
		});
	}

	function CargarTallas(){
		$.ajax({
			type:"POST",
			url:"ObtenerProductoTalla",
			data:"genero=" + $('#Genero').val(),
			success:function(r){
				$('#Talla').html(r);
			}
		});
	}
	function CargaTipoMangas(){
		$.ajax({
			type:"POST",
			url:"ObtenerProductoTipoManga",
			data:"idtipoproducto=" + $('#IDTipoProducto').val(),
			success:function(r){
				$('#TipoManga').html(r);
			}
		});
	}
	function CargaTipoCuellos(){
		$.ajax({
			type:"POST",
			url:"ObtenerProductoTipoCuello",
			data:"tipomanga=" + $('#TipoManga').val(),
			success:function(r){
				$('#TipoCuello').html(r);
			}
		});
	}	
	function CargarIDProductoFinal(){
		$.ajax({
			type:"POST",
			url:"ObtenerIDProducto",
			data:{idtipoproducto : $('#IDTipoProducto').val(),
					genero :  $('#Genero').val() ,
					talla :  $('#Talla').val() ,
					tipomanga :  $('#TipoManga').val() ,
					tipocuello :  $('#TipoCuello').val() 
				  },
			success:function(r){
				$("#IDProductoFinal").html(r);
				
			}
		});
	}	
	function InsertarPedido1(){
		$.ajax({
			type:"POST",
			url:"InsertarPedido1",
			data:{folio : $('#Folio').val() ,
					idCliente :  $('#IDCliente').val() ,
					fechaIngreso :  $('#FechaIngreso').val() ,
					fechaEntrega :  $('#FechaEntrega').val() ,
					tipoTrabajo :  $('#TipoTrabajo').val() ,
					pedidoExpress :  $('#PedidoExpress').val() ,
					ingresadoPor :  $('#IngresadoPor').val() ,
					estadoPedido :  $('#EstadoPedido').val() ,
				  },
		});
	}	
	 
</script>

<script>  
	$(document).ready(function() { 
	var count = 0;
	var bootstrapButton = $.fn.button.noConflict() // Visualizar btn cerra 
	$.fn.bootstrapBtn = bootstrapButton  
		$('#DetalleProductoLista').dialog({
			autoOpen:false,
			width:400
		});

		$('#Agregar').click(function(){
			$('#DetalleProductoLista').dialog('option', 'title', 'Agregar Datos');
			$('#Cantidad').val('');
			$('#Genero').val('');
			$('#Talla').val('');
			$('#IDProductoFinal').val('');
			$('#error_cantidad').text('');
			$('#error_genero').text('');
			$('#error_talla').text('');
			$('#Cantidad').css('border-color', '');
			$('#Genero').css('border-color', '');
			$('#Talla').css('border-color', '');
			$('#GuardarDetalleProducto').text('Guardar');
			$('#DetalleProductoLista').dialog('open');
		});

			$('#GuardarDetalleProducto').click(function(){
				var error_cantidad = '';
				var error_genero = '';
				var cantidad = '';
				var genero = '';
				var talla = '';
				var idproductofinal = '';
				if($('#Cantidad').val() == '') {
					error_cantidad = 'Cantidad es requerida';
					$('#error_cantidad').text(error_cantidad);
					$('#Cantidad').css('border-color', '#cc0000');
					cantidad = '';
				}
				else {
					error_cantidad = '';
					$('#error_cantidad').text(error_cantidad);
					$('#Cantidad').css('border-color', '');
					cantidad = $('#Cantidad').val();
				} 
				if($('#Genero').val() == null) {
					error_genero = 'genero es requerido';
					$('#error_genero').text(error_genero);
					$('#Genero').css('border-color', '#cc0000');
					genero = '';
				} else {
					error_genero = '';
					$('#error_genero').text(error_genero);
					$('#Genero').css('border-color', '');
					genero = $('#Genero').val();
				}
				if($('#Talla').val() == null) {
					error_talla = 'talla es requerido';
					$('#error_talla').text(error_talla);
					$('#Talla').css('border-color', '#cc0000');
					talla = '';
				} else {
					error_talla = '';
					$('#error_talla').text(error_talla);
					$('#Talla').css('border-color', '');
					talla = $('#Talla').val();
					idproductofinal = $('#IDProductoFinal').val();
				}
				if(error_cantidad != '' || error_genero != '' || error_talla != '') {
					return false;
				} else {
				if($('#GuardarDetalleProducto').text() == 'Guardar') {
					count = count + 1;
					output = '<tr id="row_'+count+'">';
					output += '<td>'+cantidad+' <input type="hidden" name="hidden_cantidad[]" id="Cantidad'+count+'" class="cantidad" value="'+cantidad+'" /></td>';
					output += '<td>'+genero+' <input type="hidden" name="hidden_genero[]" id="Genero'+count+'" value="'+genero+'" /></td>';
					output += '<td>'+talla+' <input type="hidden" name="hidden_talla[]" id="Talla'+count+'" value="'+talla+'" /></td>';
					output += '<td>'+idproductofinal+' <input type="hidden" name="hidden_idproductofinal[]" id="IDProductoFinal'+count+'" value="'+idproductofinal+'" /></td>';
					output += '<td><button type="button" name="view_details" class="btn btn-warning btn-xs view_details btn-circle " id="'+count+'"><i class="fa fa-pencil"></i></button><button type="button" name="remove_details" class="btn btn-danger btn-xs remove_details btn-circle " id="'+count+'"><i class="fa fa-trash-o"></i></button></td>';
					output += '</tr>';
					$('#user_data').append(output);
				}
				else {
					var row_id = $('#hidden_row_id').val();
					output = '<td>'+cantidad+' <input type="hidden" name="hidden_cantidad[]" id="Cantidad'+row_id+'" class="cantidad" value="'+cantidad+'" /></td>';
					output += '<td>'+genero+' <input type="hidden" name="hidden_genero[]" id="Genero'+row_id+'" value="'+genero+'" /></td>';
					output += '<td>'+talla+' <input type="hidden" name="hidden_talla[]" id="Talla'+row_id+'" value="'+talla+'" /></td>';
					output += '<td>'+idproductofinal+' <input type="hidden" name="hidden_idproductofinal[]" id="IDProductoFinal'+row_id+'" value="'+idproductofinal+'" /></td>';
					output += '<td><button type="button" name="view_details" class="btn btn-warning btn-xs view_details btn-circle" id="'+row_id+'"><i class="fa fa-pencil"></i></button><button type="button" name="remove_details" class="btn btn-danger btn-xs remove_details btn-circle " id="'+row_id+'"><i class="fa fa-trash-o"></i></button></td>';
					$('#row_'+row_id+'').html(output);
				}
				$('#DetalleProductoLista').dialog('close');
			}
	});

	$(document).on('click', '.view_details', function(){
		var row_id = $(this).attr("id");
		var cantidad = $('#Cantidad'+row_id+'').val();
		var genero = $('#Genero'+row_id+'').val();
		var talla = $('#Talla'+row_id+'').val();
		var idproductofinal = $('#IDProductoFinal'+row_id+'').val();
		$('#Cantidad').val(cantidad);
		$('#Genero').val(genero);
		$('#Talla').val(talla);
		$('#IDProductoFinal').val(idproductofinal);
		$('#GuardarDetalleProducto').text('Editar');
		$('#hidden_row_id').val(row_id);
		$('#DetalleProductoLista').dialog('option', 'title', 'Editar Datos');
		$('#DetalleProductoLista').dialog('open');
	});

	$(document).on('click', '.remove_details', function(){
		var row_id = $(this).attr("id");
		$('#row_'+row_id+'').remove();
	});

	$('#action_alert').dialog({
		autoOpen:false
	});

	$('#user_form').on('submit', function(event){
		event.preventDefault();
		var count_data = 0;
		$('.cantidad').each(function(){
			count_data = count_data + 1;
		});
		if(count_data > 0) {
			var form_data = $(this).serialize();
			$.ajax({
				url:"Uploaddetalleproducto",
				method:"POST",
				data:form_data,
				success:function(data) {
					$('#user_data').find("tr:gt(0)").remove();
					$('#action_alert').html('<p>Datos Registrados correctamente</p>');
					$('#action_alert').dialog('open');
				}
			})
		} else {
		$('#action_alert').html('<p>error</p>');
		$('#action_alert').dialog('open');
		}
	});
});  
</script>

