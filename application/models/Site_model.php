<?php
	class Site_model extends CI_Model{

		public function loginUser($data){
			$this->db->select("*");
			$this->db->from("Trabajador");
			$this->db->where("area",'Externo');
			$this->db->where("activo",'1');
			$this->db->where("username",$data['username']);
			$this->db->where("password",$data['password']);

			$query=$this->db->get();
			if($query->num_rows()>0){
				return $query->result();
			}else{ 		 
				$this->db->select("*");
				$this->db->from("Trabajador");
				$this->db->where("area",'Corte');
				$this->db->where("activo",'1');
				$this->db->where("username",$data['username']);
				$this->db->where("password",$data['password']);

				$queryCorte=$this->db->get();
				if($queryCorte->num_rows()>0){
					return $queryCorte->result();
				}else{
					
					$this->db->select("*");
					$this->db->from("Trabajador");
					$this->db->where("area",'Taller');
					$this->db->where("activo",'1');
					$this->db->where("username",$data['username']);
					$this->db->where("password",$data['password']);

					$queryTaller=$this->db->get();
					if($queryTaller->num_rows()>0){
						return $queryTaller->result();
					}else{
						$this->db->select("*");
						$this->db->from("Trabajador");
						$this->db->where("area",'Admin');
						$this->db->where("activo",'1');
						$this->db->where("username",$data['username']);
						$this->db->where("password",$data['password']);

						$queryAdmin=$this->db->get();
						if($queryAdmin->num_rows()>0){
							return $queryAdmin->result();
						}else{
							
							$this->db->select("*");
							$this->db->from("Trabajador");
							$this->db->where("area",'Secretaria');
							$this->db->where("activo",'1');
							$this->db->where("username",$data['username']);
							$this->db->where("password",$data['password']);

							$querySecretaria=$this->db->get();
							if($querySecretaria->num_rows()>0){
								return $querySecretaria->result();
							}else{
								
								return NULL;
									
							}
								
						}	
					}			
				}		
			}	
		}	

		function getTrabajadores(){
			$this->db->select("*");
			$this->db->from("Trabajador");
			$this->db->where("Activo",'1');

			$query=$this->db->get();
		//	print_r($this->db->last_query());
			if($query->num_rows()>0){
				return $query->result();
			}else{
				return NULL;	
			}
		}

		function deleteTrabajador($ID_Trabajador){
			$array=array(
				"Activo" => 0
			);
			$this->db->where("ID_Trabajador",$ID_Trabajador);
			$this->db->update("Trabajador",$array);
		}	
		function getProductos(){
			$this->db->select("*");
			$this->db->from("TipoProducto");
			$this->db->where("Disponible",'1');
			$query=$this->db->get();
		//	print_r($this->db->last_query());
			if($query->num_rows()>0){
				return $query->result();
			}else{
				return NULL;	
			}
		}

		function deleteProducto($ID_TipoProducto){
			$array=array(
				"Disponible" => 0
			);
			$this->db->where("ID_TipoProducto",$ID_TipoProducto);
			$this->db->update("TipoProducto",$array);
		}	

		function getColor(){
			$this->db->select("*");
			$this->db->from("Color");
			$this->db->where("Disponible",'1');

			$query=$this->db->get();
		//	print_r($this->db->last_query());
			if($query->num_rows()>0){
				return $query->result();
			}else{
				return NULL;	
			}
		}

		function deleteColor($ID_Color){
			$array=array(
				"Disponible" => 0
			);
			$this->db->where("ID_Color",$ID_Color);
			$this->db->update("Color",$array);
		}	


		function getEstiloEquipo(){
			$this->db->select("*");
			$this->db->from("EstiloEquipo");
			$this->db->where("Disponible",'1');

			$query=$this->db->get();
		//	print_r($this->db->last_query());
			if($query->num_rows()>0){
				return $query->result();
			}else{
				return NULL;	
			}
		}

		function deleteEstiloEquipo($ID_EstiloEquipo){
			$array=array(
				"Disponible" => 0
			);
			$this->db->where("ID_EstiloEquipo",$ID_EstiloEquipo);
			$this->db->update("EstiloEquipo2",$array);
		}	

		function uploadCliente($data){

			$array = array(
				"Nombre"=> $data['nombre'],
				"Apellido"=> $data['apellido'],
				"Rut"=> $data['rut'],
				"Telefono"=> $data['telefono'],
				"Correo"=> $data['email'],
			);
			$this->db->insert("Cliente",$array);
		}
		

		function getCliente(){
			$this->db->select("*");
			$this->db->from("Cliente");

			$query=$this->db->get();
		//	print_r($this->db->last_query());
			if($query->num_rows()>0){
				return $query->result();
			}else{
				return NULL;	
			}
		}

		function uploadColor($data){

			$array = array(
				"Color"=> $data['color'],
				"Codigo"=> $data['codigoColor'],
			);
			$this->db->insert("Color",$array);
		}

		function uploadEstiloEquipo($data){

			$array = array(
				"Nombre"=> $data['nombre'],
			);
			$this->db->insert("EstiloEquipo2",$array);
		}

		function uploadProducto($data){

			$array = array(
				"Nombre"=> $data['nombre'],
			);
			$this->db->insert("TipoProducto",$array);
		}

		function uploadOrden($data,$archivo=null){
				$array = array(
					"ID_Cliente"=> $data['idCliente'],
					"Folio"=> $data['folio'],
					"TipoTrabajo"=> $data['tipoTrabajo'],
					"FechaIngreso"=> $data['fechaIngreso'],
					"FechaEntrega"=> $data['fechaEntrega'],
					"PedidoExpress"=> $data['pedidoExpress'],
					"IngresadoPor"=> $data['ingresadoPor'],
					"EstadoPedido"=> $data['estadoPedido']
				);

			$this->db->insert("OrdenDeCompra",$array);

		}

		function uploadOrdenProducto($data){

			$array = array(
				"ID_TipoProducto"=> $data['producto'],
				"Genero"=> $data['genero'],
				"Talla"=> $data['talla'],
				"TipoManga"=> $data['tipoManga'],
				"TipoCuello"=> $data['tipoCuello'],
				"PedidoExpress"=> $data['pedidoExpress'],
				"IngresadoPor"=> $data['ingresadoPor'],
				"EstadoPedido"=> $data['estadoPedido'],
			);
			$this->db->insert("Producto",$array);
		}
		
		function uploadOrdenProductogenero($data){

			$array = array(
				"ID_TipoProducto"=> $data['idtipoproducto'],
				"Genero"=> $data['genero'],
				"Talla"=> $data['talla'],
				"TipoManga"=> $data['tipomanga'],
				"TipoCuello"=> $data['tipocuello'],

			);
			$this->db->insert("Producto",$array);
		
		}

			function getOrdenProductoNombre(){		
					$this->db->select("*");
					$this->db->from("TipoProducto");
					print_r($this->db->last_query());
					$query=$this->db->get();
					if($query->num_rows()>0){
						return $query->result();
					}else{
						return NULL;	
					}
				
			}

			function getOrdenProductoGenero(){
					$tipocuello= $_POST['tipocuello'];	
					$this->db->select("Genero");
					$this->db->distinct('Genero');
					$this->db->from("Producto");
					$this->db->join('TipoProducto', 'Producto.ID_TipoProducto = TipoProducto.ID_TipoProducto','INNER');
					$this->db->where("TipoCuello",$tipocuello);
					$query=$this->db->get();
					print_r($this->db->last_query());
					if($query->num_rows()>0){
						return $query->result();
					}else{
						return NULL;	
					}
			}


			
			function getOrdenProductoTalla(){
					$genero= $_POST['genero'];	
					$this->db->select("Talla");
					$this->db->distinct('Talla');
					$this->db->from("Producto");
					$this->db->join('TipoProducto', 'Producto.ID_TipoProducto = TipoProducto.ID_TipoProducto','INNER');
					$this->db->where("Genero",$genero);
					$query=$this->db->get();
			//		print_r($this->db->last_query());
					if($query->num_rows()>0){
						return $query->result();
					}else{
						return NULL;	
					}
			}

			function getOrdenProductoTipoManga(){
				$idtipoproducto= $_POST['idtipoproducto'];	
				$this->db->select("TipoManga");
				$this->db->distinct('TipoManga');
				$this->db->from("Producto");
				$this->db->where("ID_TipoProducto",$idtipoproducto);
				$query=$this->db->get();
				print_r($this->db->last_query());
				if($query->num_rows()>0){
					return $query->result();
				}else{
					return NULL;	
				}
			}

			function getOrdenProductoTipoCuello(){
				$tipomanga= $_POST['tipomanga'];	
				$this->db->select("TipoCuello");
				$this->db->distinct('TipoCuello');
				$this->db->from("Producto");
				$this->db->join('TipoProducto', 'Producto.ID_TipoProducto = TipoProducto.ID_TipoProducto','INNER');
				$this->db->where("TipoManga",$tipomanga);
				$query=$this->db->get();
			//	print_r($this->db->last_query());
				if($query->num_rows()>0){
					return $query->result();
				}else{
					return NULL;	
				}
		}

			function getOrdenIDProducto(){
				$idtipoproducto=$_POST['idtipoproducto'];
				$tipocuello=$_POST['tipocuello'];
				$tipomanga=$_POST['tipomanga'];
				$talla=$_POST['talla'];
				$genero=$_POST['genero'];
				$this->db->select("ID_Producto");
				$this->db->from("Producto");
				$this->db->where("ID_TipoProducto",$idtipoproducto);
				$this->db->where("TipoCuello",$tipocuello);
				$this->db->where("TipoManga",$tipomanga);
				$this->db->where("Talla",$talla);
				$this->db->where("Genero",$genero);
				
				$query=$this->db->get();
				print_r($this->db->last_query());
				if($query->num_rows()>0){
					return $query->result();
				}else{
					return NULL;	
				}
			}

/*
			function getOrdenProductoGenero(){
				$this->db->select("Genero");
				$this->db->distinct('Genero');
				$this->db->from("Producto");
				$this->db->join('TipoProducto', 'Producto.ID_TipoProducto = TipoProducto.ID_TipoProducto','LEFT');
				$query=$this->db->get();
			//	print_r($this->db->last_query());
				if($query->num_rows()>0){
					return $query->result();
				}else{
					return NULL;	
				}
			}

			function getOrdenProductoTalla(){
				$this->db->select("Talla");
				$this->db->distinct('Talla');
				$this->db->from("Producto");
				$this->db->join('TipoProducto', 'Producto.ID_TipoProducto = TipoProducto.ID_TipoProducto','LEFT');
				$query=$this->db->get();
		//		print_r($this->db->last_query());
				if($query->num_rows()>0){
					return $query->result();
				}else{
					return NULL;	
				}
			}
			
			function getOrdenProductoTipoManga(){
				$this->db->select("TipoManga");
				$this->db->distinct('TipoManga');
				$this->db->from("Producto");
				$this->db->join('TipoProducto', 'Producto.ID_TipoProducto = TipoProducto.ID_TipoProducto','LEFT');
				$query=$this->db->get();
				print_r($this->db->last_query());
				if($query->num_rows()>0){
					return $query->result();
				}else{
					return NULL;	
				}
			}
			function getOrdenProductoTipoCuello(){
				$this->db->select("TipoCuello");
				$this->db->distinct('TipoCuello');
				$this->db->from("Producto");
				$this->db->join('TipoProducto', 'Producto.ID_TipoProducto = TipoProducto.ID_TipoProducto','LEFT');
				$query=$this->db->get();
		//		print_r($this->db->last_query());
				if($query->num_rows()>0){
					return $query->result();
				}else{
					return NULL;	
				}
			}

			function getGeneroTalla($genero){
				$this->db->select("Talla");
				$this->db->distinct('Talla');
				$this->db->from("Producto");
				$this->db->join('TipoProducto', 'Producto.ID_TipoProducto = TipoProducto.ID_TipoProducto','LEFT');
				$this->db->where("Genero",$genero);
				$query=$this->db->get();
				print_r($this->db->last_query());
				if($query->num_rows()>0){
					return $query->result();
				}else{
					return NULL;	
				}
			}

	/*
	
*/
/*
		function uploadOrden($data,$archivo=null){

			if($archivo){
				$array = array(
					"Folio"=> $data['folio'],
					"ID_Cliente"=> $data['idCliente'],
					"Fecha_Ingreso"=> $data['fechaIngreso'],
					"Fecha_Enrtega"=> $data['fechaEntrega'],
					"Archivo"=> $archivo,
					"Area"=> $data['area'],
				);
			}else{
				$array = array(
					"ID_Cliente"=> $data['idCliente'],
					"Folio"=> $data['folio'],
					"TipoTrabajo"=> $data['tipoTrabajo'],
					"FechaIngreso"=> $data['fechaIngreso'],
					"FechaEntrega"=> $data['fechaEntrega'],
					"PedidoExpress"=> $data['pedidoExpress'],
					"IngresadoPor"=> $data['ingresadoPor'],
					"EstadoPedido"=> $data['estadoPedido'],
				);
			}
  
			$this->db->insert("OrdenDeCompra",$array);

		}
*/
		function getOrdenes($area){
			$this->db->select("*");
			$this->db->from("Tarea");
			$this->db->where("Area",$area);
			$this->db->order_by("Fecha_final","ASC");

			$query=$this->db->get();
		//	print_r($this->db->last_query());
			if($query->num_rows()>0){
				return $query->result();
			}else{
				return NULL;	
			}
		
		}
	
		public function getGeneros(){
			$generos = $this->db->get("Genero");
			print_r($this->db->last_query());
			if($generos->num_rows()>0){
				return $generos->result();
			}
		}

		public function getTallas($tipo){
			$this->db->select("*");
			$this->db->from("Talla");
			$this->db->where("IDGenero",$tipo);
			$query=$this->db->get();
			if($query->num_rows()>0){
				return $query->result_array();
			}else{
				return NULL;	
			}
		}
		
		public function postTabla($data){
			$array = array(
				"Producto"=> $data['producto'],
				"Articulo"=> $data['articulo'],
				"Precio"=> $data['precio'],
			);
			$this->db->insert("Cliente",$array);
		}

		function getSerie(){
			$this->db->select_Max("Folio");
			$this->db->from("OrdenDeCompra");
			$query=$this->db->get();
		//	print_r($this->db->last_query());
			if($query->num_rows()>0){
				return $query->result();
			}else{
				return NULL;	
			}
		}


		function uploadDetalleProducto($data,$archivo=null){
			$data.serialize();
			for($count = 0; $count<count($_POST['hidden_cantidad']); $count++) {
				$data = array(
				'Cantidad' => $_POST['hidden_cantidad'][$count],
				'Modelaje' => $_POST['hidden_genero'][$count],
				'Talla' => $_POST['hidden_talla'][$count]
				);
				
				$this->db->insert("DetalleProducto",$data);
			}
		}
		function insert($data,$archivo=null){
			$data.serialize();
			for($count = 0; $count<count($_POST['hidden_cantidad']); $count++) {
				$data = array(
				'Cantidad' => $_POST['hidden_cantidad'][$count],
				'Modelaje' => $_POST['hidden_genero'][$count],
				'Talla' => $_POST['hidden_talla'][$count]
				);
				
				$this->db->insert("DetalleProducto",$data);
			}
		}

		function uploadProducto1($data){

			$array = array(
				"Folio"=> $data['folio'],
				"ID_Cliente"=> $data['idCliente'],
				"FechaIngreso"=> $data['fechaIngreso'],
				"FechaEntrega"=> $data['fechaEntrega'],
				"TipoTrabajo"=> $data['tipoTrabajo'],
				"PedidoExpress"=> $data['pedidoExpress'],
				"IngresadoPor"=> $data['ingresadoPor'],
				"EstadoPedido"=> $data['estadoPedido']
			);
			$this->db->insert("OrdenDeCompra",$array);
		
		}

		

	}
?>


