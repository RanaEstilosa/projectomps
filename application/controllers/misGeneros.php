<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class misGeneros extends CI_Controller {
		public function index(){
		$this->load->model(‘modeloGenero’);
		$data[‘generos’] = $this->modeloGenero->getGeneros(); 
		$this->load->view(‘formularioGenero’,$data);
		}

		public function selectMarca(){
			$tipo=$this->input->post('genero'); if($tipo){
			$this->load->model(‘modeloGenero’);
			$marcas=$this->modeloGenero->getMarcas($tipo); foreach($marcas as $marca){
			echo "<option value='".$marca['ID_MARCA']."'>".$marca['NOMBRE_MARCA']."</opction> ";
			} }
		}
	}
