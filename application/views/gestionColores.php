<section id="main-content">
    <section class="wrapper"> 
		<div class="row mt">
			<div class="col-lg-12">
				<div class="form-panel">
					<h4 class="mb"><i class="fa fa-angle-right"></i>Registrar nuevo color </h4>
					<form action="" method="post" class="form-horizontal style-form" data-toggle="validator" role="form">
						<div class="form-group">
							<label class="col-sm-2 col-sm-2 control-label" for="inputColor" >Color</label>
							<div class="col-sm-10">
							<input type="text" class="form-control" id="inputColor" name="color" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 col-sm-2 control-label" for="inputCodigoColor" >CodigoColor</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="inputCodigoColor" name="codigoColor" required>
							</div>
						</div>
						<button type="SUBMIT" class="btn btn-info">Enviar</button>
					</form>
				</div>
				<table id="ListaColores" class="display">
					<thead>
						<tr>
							<th>ID</th>
							<th>Color</th>
							<th>Codigo Color</th>
							<th>Disponibilidad</th>
							<th>Editar</th>
						</tr>
					</thead>
					<tbody>
						<?php
						foreach ($colores as $color) {
						
						?>
						<tr id="rowColor<?= $color->ID_Color?>">
							<td> <?= $color->ID_Color?> </td>
							<td> <?= $color->Color?> </td>
							<td> <?= $color->Codigo?> </td>
							<td> <?= $color->Disponible?> </td>
							<td> <button type="button" id="color-<?= $color->ID_Color ?>"class="eliminar btn btn-danger"><i class="fa fa-trash-o"></i></button> </td>
						</tr>

						<?php
						}
						?>
					</tbody>
				</table>
			</div>
		</div>	
	</section>
</section> 

<script type="text/javascript">
// desde la vista llamaremos al controlador para eliminar
	$(".eliminar").click(function(){

		var IDColor=this.id;
		var res=ID_Color = IDColor.split("-");
		var ID_Color = res[1];
		console.log(ID_Color);

		$.post("<?= base_url() ?>Dashboard/eliminarColor", { IDColor: ID_Color }).done(function(data){
			$("#rowColor"+ID_Color).fadeOut(); // hace que se oculte
		});

	});
		


</script>

<script>
	$(document).ready( function () {
    	$('#ListaColores').DataTable();
} );
</script>
