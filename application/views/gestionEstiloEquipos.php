<section id="main-content">
    <section class="wrapper"> 
		<div class="row mt">
			<div class="col-lg-12">
				<div class="form-panel">
					<h4 class="mb"><i class="fa fa-angle-right"></i>Registrar estilo equipo </h4>
					<form action="" method="post" class="form-horizontal style-form" data-toggle="validator" role="form">
						<div class="form-group">
							<label class="col-sm-2 col-sm-2 control-label" for="inputNombre" >Nombre</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="inputNombre" name="nombre" required>
							</div>
						</div>
						<button type="SUBMIT" class="btn btn-info">Enviar</button>
					</form>
				</div>
				<table id="ListaEstiloEquipos" class="display">
					<thead>
						<tr>
							<th>ID</th>
							<th>Nombre</th>
							<th>Disponibilidad</th>
							<th>Editar</th>
						</tr>
					</thead>
					<tbody>

						<?php
						foreach ($estiloequipos as $estiloequipo) {
						
						?>
						<tr id="rowEstiloEquipo<?= $estiloequipo->ID_EstiloEquipo?>">
							<td> <?= $estiloequipo->ID_EstiloEquipo?> </td>
							<td> <?= $estiloequipo->Nombre?> </td>
							<td> <?= $estiloequipo->Disponible?> </td>
							<td> <button type="button" id="estiloequipo-<?= $estiloequipo->ID_EstiloEquipo ?>"class="eliminar btn btn-danger"><i class="fa fa-trash-o"></i></button> </td>
						</tr>

						<?php
						}
						?>
					</tbody>
				</table>
			</div>
		</div>			
	</section>
</section> 

<script type="text/javascript">
// desde la vista llamaremos al controlador para eliminar
	$(".eliminar").click(function(){

		var IDEstiloEquipo=this.id;
		var res=ID_EstiloEquipo = IDEstiloEquipo.split("-");
		var ID_EstiloEquipo = res[1];
		console.log(ID_EstiloEquipo);

		$.post("<?= base_url() ?>Dashboard/eliminarEstiloEquipo", { IDEstiloEquipo: ID_EstiloEquipo }).done(function(data){
			$("#rowEstiloEquipo"+ID_EstiloEquipo).fadeOut(); // hace que se oculte
		});

	});

</script>

<script>
	$(document).ready( function () {
    	$('#ListaEstiloEquipos').DataTable();
} );
</script>
