<html>  
    <head>  
        <title>PHP - Sending multiple forms data through jQuery Ajax</title>  
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="bootstrap.min.css" />
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    </head>  
    <body>  
        <div class="container">
   <br />
   
   <h3 align="center">PHP - Sending multiple forms data through jQuery Ajax</a></h3><br />
   <br />
   <br />
   <div align="right" style="margin-bottom:5px;">
    <button type="button" name="add" id="add" class="btn btn-success btn-xs">Add</button>
   </div>
   <br />
   <form method="post" id="user_form">
    <div class="table-responsive">
     <table class="table table-striped table-bordered" id="user_data">
      <tr>
       <th>Cantidad</th>
       <th>Genero</th>
       <th>Editar</th>
       <th>Eliminar</th>
      </tr>
     </table>
    </div>
    <div align="center">
     <input type="submit" name="insert" id="insert" class="btn btn-primary" value="Insert" />
    </div>
   </form>

   <br />
  </div>
  <div id="user_dialog" title="Add Data">
   <div class="form-group">
    <label>Ingrese cantidad</label>
    <input type="text" name="cantidad" id="cantidad" class="form-control" />
    <span id="error_cantidad" class="text-danger"></span>
   </div>
   <div class="form-group">
    <label>Genero</label>
    <input type="text" name="genero" id="genero" class="form-control" />
    <span id="error_genero" class="text-danger"></span>
   </div>
   <div class="form-group" align="center">
    <input type="hidden" name="row_id" id="hidden_row_id" />
    <button type="button" name="save" id="save" class="btn btn-info">Save</button>
   </div>
  </div>
  <div id="action_alert" title="Action">

  </div>
    </body>  
</html> 


<script>  
	$(document).ready(function() { 
	var count = 0;
	var bootstrapButton = $.fn.button.noConflict() // return $.fn.button to previously assigned value
	$.fn.bootstrapBtn = bootstrapButton  
		$('#DetalleProductoLista').dialog({
			autoOpen:false,
			width:400
		});

		$('#Agregar').click(function(){
			$('#DetalleProductoLista').dialog('option', 'title', 'Agregar Datos');
			$('#Cantidad').val('');
			$('#Genero').val('');
			$('#Talla').val('');
			$('#IDProductoFinal').val('');
			$('#error_cantidad').text('');
			$('#error_genero').text('');
			$('#error_talla').text('');
			$('#Cantidad').css('border-color', '');
			$('#Genero').css('border-color', '');
			$('#Talla').css('border-color', '');
			$('#GuardarDetalleProducto').text('Guardar');
			$('#DetalleProductoLista').dialog('open');
		});

			$('#GuardarDetalleProducto').click(function(){
				var error_cantidad = '';
				var error_genero = '';
				var cantidad = '';
				var genero = '';
				var talla = '';
				var idproductofinal = '';
				if($('#Cantidad').val() == '') {
					error_cantidad = 'Cantidad es requerida';
					$('#error_cantidad').text(error_cantidad);
					$('#Cantidad').css('border-color', '#cc0000');
					cantidad = '';
				}
				else {
					error_cantidad = '';
					$('#error_cantidad').text(error_cantidad);
					$('#Cantidad').css('border-color', '');
					cantidad = $('#Cantidad').val();
				} 
				if($('#Genero').val() == null) {
					error_genero = 'genero es requerido';
					$('#error_genero').text(error_genero);
					$('#Genero').css('border-color', '#cc0000');
					genero = '';
				} else {
					error_genero = '';
					$('#error_genero').text(error_genero);
					$('#Genero').css('border-color', '');
					genero = $('#Genero').val();
				}
				if($('#Talla').val() == null) {
					error_talla = 'talla es requerido';
					$('#error_talla').text(error_talla);
					$('#Talla').css('border-color', '#cc0000');
					talla = '';
				} else {
					error_talla = '';
					$('#error_talla').text(error_talla);
					$('#Talla').css('border-color', '');
					talla = $('#Talla').val();
					idproductofinal = $('#IDProductoFinal').val();
				}
				if(error_cantidad != '' || error_genero != '' || error_talla != '') {
					return false;
				} else {
				if($('#GuardarDetalleProducto').text() == 'Guardar') {
					count = count + 1;
					output = '<tr id="row_'+count+'">';
					output += '<td>'+cantidad+' <input type="hidden" name="hidden_cantidad[]" id="Cantidad'+count+'" class="cantidad" value="'+cantidad+'" /></td>';
					output += '<td>'+genero+' <input type="hidden" name="hidden_genero[]" id="Genero'+count+'" value="'+genero+'" /></td>';
					output += '<td>'+talla+' <input type="hidden" name="hidden_talla[]" id="Talla'+count+'" value="'+talla+'" /></td>';
					output += '<td>'+idproductofinal+' <input type="hidden" name="hidden_idproductofinal[]" id="IDProductoFinal'+count+'" value="'+idproductofinal+'" /></td>';
					output += '<td><button type="button" name="view_details" class="btn btn-warning btn-xs view_details btn-circle " id="'+count+'"><i class="fa fa-pencil"></i></button><button type="button" name="remove_details" class="btn btn-danger btn-xs remove_details btn-circle " id="'+count+'"><i class="fa fa-trash-o"></i></button></td>';
					output += '</tr>';
					$('#user_data').append(output);
				}
				else {
					var row_id = $('#hidden_row_id').val();
					output = '<td>'+cantidad+' <input type="hidden" name="hidden_cantidad[]" id="Cantidad'+row_id+'" class="cantidad" value="'+cantidad+'" /></td>';
					output += '<td>'+genero+' <input type="hidden" name="hidden_genero[]" id="Genero'+row_id+'" value="'+genero+'" /></td>';
					output += '<td>'+talla+' <input type="hidden" name="hidden_talla[]" id="Talla'+row_id+'" value="'+talla+'" /></td>';
					output += '<td>'+idproductofinal+' <input type="hidden" name="hidden_idproductofinal[]" id="IDProductoFinal'+row_id+'" value="'+idproductofinal+'" /></td>';
					output += '<td><button type="button" name="view_details" class="btn btn-warning btn-xs view_details btn-circle" id="'+row_id+'"><i class="fa fa-pencil"></i></button><button type="button" name="remove_details" class="btn btn-danger btn-xs remove_details btn-circle " id="'+row_id+'"><i class="fa fa-trash-o"></i></button></td>';
					$('#row_'+row_id+'').html(output);
				}
				$('#DetalleProductoLista').dialog('close');
			}
	});

	$(document).on('click', '.view_details', function(){
		var row_id = $(this).attr("id");
		var cantidad = $('#Cantidad'+row_id+'').val();
		var genero = $('#Genero'+row_id+'').val();
		var talla = $('#Talla'+row_id+'').val();
		var idproductofinal = $('#IDProductoFinal'+row_id+'').val();
		$('#Cantidad').val(cantidad);
		$('#Genero').val(genero);
		$('#Talla').val(talla);
		$('#IDProductoFinal').val(idproductofinal);
		$('#GuardarDetalleProducto').text('Editar');
		$('#hidden_row_id').val(row_id);
		$('#DetalleProductoLista').dialog('option', 'title', 'Editar Datos');
		$('#DetalleProductoLista').dialog('open');
	});

	$(document).on('click', '.remove_details', function(){
		var row_id = $(this).attr("id");
		$('#row_'+row_id+'').remove();
	});

	$('#action_alert').dialog({
		autoOpen:false
	});

	$('#user_form').on('submit', function(event){
		event.preventDefault();
		var count_data = 0;
		$('.cantidad').each(function(){
			count_data = count_data + 1;
		});
		if(count_data > 0) {
			var form_data = $(this).serialize();
			$.ajax({
				url:"Uploaddetalleproducto",
				method:"POST",
				data:form_data,
				success:function(data) {
					$('#user_data').find("tr:gt(0)").remove();
					$('#action_alert').html('<p>Datos Registrados correctamente</p>');
					$('#action_alert').dialog('open');
				}
			})
		} else {
		$('#action_alert').html('<p>error</p>');
		$('#action_alert').dialog('open');
		}
	});
});  
</script>

