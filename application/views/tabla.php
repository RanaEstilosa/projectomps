<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="../css/bootstrap-theme.css">
		<link rel="stylesheet" type="text/css" href="../estilo.css">
		<link rel="stylesheet" type="text/css" href="../font-awesome/css/font-awesome.css">
		<script src="../js/jquery-1.11.3.min.js" type="text/javascript"></script>
		<script src="http://code.jquery.com/jquery-latest.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){

		$("#add").click(function(){
		// Obtenemos el numero de columnas (td) que tiene la primera fila
		// (tr) del id "tabla"
		var tds=$("#tabla tr:first td").length;
		// Obtenemos el total de filas (tr) del id "tabla"
		var trs=$("#tabla tr").length;
		var nuevaFila="<tr>";
		cant = $('#contador-filas').val();
		cant++;
		$('#contador-filas').val(cant)
		nuevaFila+="<td><input class='form-control' type='text' name='cantidad["+(cant)+"]' placeholder='cantidad"+(cant)+"' required /> </td>"+
		"<td><input class='form-control' type='text' name='articulo["+(cant)+"]' placeholder='articulo"+(cant)+"' required /> </td>"+
		"<td><input class='form-control' type='text' name='precio["+(cant)+"]' placeholder='precio"+(cant)+"' required /> </td>";
		// Añadimos una columna con el numero total de columnas.
		// Añadimos uno al total, ya que cuando cargamos los valores para la
		// columna, todavia no esta añadida
		nuevaFila+="</tr>";
		$("#tabla").append(nuevaFila);
		});
		/**
		* Funcion para eliminar la ultima columna de la tabla.
		* Si unicamente queda una columna, esta no sera eliminada
		*/
		$("#del").click(function(){
		// Obtenemos el total de filas (tr) del id "tabla"
		var trs=$("#tabla tr").length;
		if(trs>2)
		{
		// Eliminamos la ultima fila
		cant--;
		$('#contador-filas').val(cant)
		$("#tabla tr:last").remove();

		}
		});
		});
		</script>

		<style>
			#add, #del {text-decoration:none;color:#fff;}
		</style>
	</head>

	<body>
		<?php
		if (!isset($_POST['datos'])) 
		{ 
		?>
		<div class="container-fluid">
			<div class="row">
				<center>
				<h2>Tabla Dinamica</h2>
				<br /><br />
				<button id="add" class="btn btn-sm btn-success">Agregar</button>
				<button id="del" class="btn btn-sm btn-danger">Eliminar</button>
				<br /><br />
			<form action="" method="post">
				<table class="table table-hover table-condensed" style="width: 40%" id="tabla">
				<thead>
					<tr class="info">
					<td><b>campo 1</b></td>
					<td><b>campo 2</b></td>
					<td><b>campo 3</b></td>
					</tr>
				</thead>
				<tbody>
					<tr class="fila-0">
						<input type="hidden" id="contador-filas" value="0" />
					<td><input class="form-control" type="text" name="cantidad[0]" placeholder="cantidad" required /></td>
					<td><input class="form-control" type="text" name="articulo[0]" placeholder="articulo" required /></td>
					<td><input class="form-control" type="text" name="precio[0]" placeholder="precio" required /></td>
					</tr>
				</tbody>
				</table>
			<button type="submit" name="datos" class="btn btn-sm btn-primary" >Guardar </button>
			<button type="reset" name="borrar" class="btn btn-sm btn-warning" >Limpiar </button>
			</form>
				</center>
		<?php
			}
		else {
		$cantidad[]= $_POST['cantidad'];
		$articulo[]= $_POST['articulo'];
		$precio[]= $_POST['precio'];
		} 
		?>

			</div>
		</div>
	</body>
</html>
