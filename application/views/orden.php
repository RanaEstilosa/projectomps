<section id="main-content">
    <section class="wrapper"> 
		<div class="row mt">
			<div class="col-lg-12">
				<div class="form-panel">
					<h4 class="mb"><i class="fa fa-angle-right"></i>Crear tarea</h4>
					<form action="" method="post" class="form-horizontal style-form" enctype="multipart/form-data">
					<div class="form-group">
						<label class="col-sm-2 col-sm-2 control-label">Nombre de la tarea</label>
						<div class="col-sm-10">
						<input type="text" class="form-control" name="nombre">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 col-sm-2 control-label">Descripción</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="descripcion">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 col-sm-2 control-label">Fecha de finalizacion</label>
						<div class="col-sm-10">
						<input type="date" name="fecha" class="form-control round-form">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 col-sm-2 control-label">Archivo adjuntos</label>
						<div class="col-sm-10">
						<input class="form-control"  type="file" name="archivo">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 col-sm-2 control-label">Area</label>
						<div class="col-sm-10">
							<Select name="area">
								<option value="Taller">Taller</option>
								<option value="Corte">Corte</option>
								<option value="Secretaria">Secretaria</option>
								<option value="Admin">Admin</option>
							</Select>
						</div>
					</div>
					<input type="submit" name="">
					</form>
				</div>
			</div>
		</div>	
	</section>
</section> 
