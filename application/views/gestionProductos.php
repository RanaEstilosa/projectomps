<section id="main-content">
    <section class="wrapper"> 
		<div class="row mt">
			<div class="col-lg-12">
				<div class="form-panel">
					<h4 class="mb"><i class="fa fa-angle-right"></i>Registrar nuevo Producto </h4>
					<form action="" method="post" class="form-horizontal style-form" data-toggle="validator" role="form">
						<div class="form-group">
							<label class="col-sm-2 col-sm-2 control-label" for="inputNombre" >Nombre</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="inputNombre" name="nombre" required>
							</div>
						</div>
						<button type="SUBMIT" class="btn btn-info">Enviar</button>
					</form>
				</div>
				<table id="ListaProductos" class="display">
					<thead>
						<tr>
								<th>ID</th>
								<th>Nombre</th>
								<th>Disponibilidad</th>
								
								<th>Editar</th>
						</tr>
					</thead>
					<tbody>
						<?php
						foreach ($productos as $producto) {
						?>
						<tr id="rowProducto<?= $producto->ID_TipoProducto?>">
							<td> <?= $producto->ID_TipoProducto?> </td>
							<td> <?= $producto->Nombre?> </td>
							<td> <?= $producto->Disponible?> </td>		
							<td> <button type="button" id="producto-<?= $producto->ID_TipoProducto ?>"class="eliminar btn btn-danger"><i class="fa fa-trash-o"></i></button> </td>
						</tr>

						<?php
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</section>
</section> 

<script type="text/javascript">
// desde la vista llamaremos al controlador para eliminar
	$(".eliminar").click(function(){

		var IDProducto=this.id;
		var res=ID_TipoProducto = IDProducto.split("-");
		var ID_TipoProducto = res[1];
		console.log(ID_TipoProducto);

		$.post("<?= base_url() ?>Dashboard/eliminarProducto", { IDProducto: ID_TipoProducto }).done(function(data){
			$("#rowProducto"+ID_TipoProducto).fadeOut(); // hace que se oculte
		});

	});
		


</script>

<script>
	$(document).ready( function () {
    	$('#ListaProductos').DataTable();
} );
</script>
