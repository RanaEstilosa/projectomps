
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
              	  <p class="centered"><a href="profile.html"><img src="<?=base_url()?>assets/img/ui-sam.jpg" class="img-circle" width="60"></a></p>
              	  <h5 class="centered">
				  	<?php 
						if(isset($_SESSION['Nombre']) && isset($_SESSION['Apellido'])){
							echo strtoupper( $_SESSION['Nombre']." ".$_SESSION['Apellido']);
						}
					?> 
				  </h5>
              	  	
                  <li class="mt">
				 
                      <a class="active" href="<?=base_url()?>Dashboard">
                          <i class="fa fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>

				<?php
					if($_SESSION['Area']=="Externo"){
				?>
				
				<li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-cogs"></i>
                          <span>Configurar</span>
                      </a>
                      <ul class="sub">
					  	  <li><a  href="<?=base_url()?>Dashboard/gestionProductos">Productos</a></li>
					  	  <li><a   href="<?=base_url()?>Dashboard/gestionColores">Color</a></li>
				<!--		  <li><a  href="responsive_table.html">Tipo Prenda</a></li>  -->
						  <li><a  href="responsive_table.html">Estilo Equipo</a></li>
						  <li><a  href="<?=base_url()?>Dashboard/gestionTrabajadores">Usuario</a></li>				  
                      </ul>
                  </li>

				<?php
					}
				?>

				<?php
					if($_SESSION['Area']=="Admin"){
				?>
	
				<li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-book"></i>
                          <span>Orden de Compra</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="<?= base_url()?>Dashboard/crearOrden">Nueva</a></li>
                          <li><a  href="<?= base_url()?>Dashboard/misOrden">"Todas las ordenes</a></li>
                      </ul>
				  </li>
				  
				  <li class="sub-menu">
				  		<a   href="<?=base_url()?>Dashboard/cliente">
                          <i class="fa fa-users"></i>
                          <span>Cliente</span>
                      </a>
                  </li>
				  
				  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-list"></i>
                          <span>Listado</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="blank.html">Corte</a></li>
                          <li><a  href="login.html">Sublimado</a></li>
						  <li><a  href="lock_screen.html">Costura</a></li>
						  <li><a  href="lock_screen.html">Empaquetado</a></li>
						  <li><a  href="lock_screen.html">Planchado</a></li>
						  <li><a  href="lock_screen.html">Compra</a></li>
						  <li><a  href="lock_screen.html">Finalizado</a></li>
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-cogs"></i>
                          <span>Configurar</span>
                      </a>
                      <ul class="sub">
					 	 <li><a  href="<?=base_url()?>Dashboard/gestionProductos">Productos</a></li>
						  <li><a   href="<?=base_url()?>Dashboard/gestionColores">Color</a></li>
				<!--		  <li><a   href="<?=base_url()?>Dashboard/gestionTipoPrendas">Tipo Prenda</a></li> -->
						  <li><a   href="<?=base_url()?>Dashboard/gestionEstiloEquipos">Estilo Equipo</a></li>
						  <li><a  href="<?=base_url()?>Dashboard/gestionTrabajadores">Usuario</a></li>
						  
                      </ul>
                  </li>
				<?php
					}
				?>

				<?php
					if($_SESSION['Area']=="Corte"){
				?>
				  				  
				  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-list"></i>
                          <span>Listado</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="blank.html">Corte</a></li>
						  <li><a  href="lock_screen.html">Finalizado</a></li>
                      </ul>
                  </li>
				<?php
					}
				?>
				<?php
					if($_SESSION['Area']=="Taller"){
				?>
				  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-list"></i>
                          <span>Listado</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="blank.html">Corte</a></li>
                          <li><a  href="login.html">Sublimado</a></li>
						  <li><a  href="lock_screen.html">Costura</a></li>
						  <li><a  href="lock_screen.html">Empaquetado</a></li>
						  <li><a  href="lock_screen.html">Planchado</a></li>
						  <li><a  href="lock_screen.html">Compra</a></li>
						  <li><a  href="lock_screen.html">Finalizado</a></li>
                      </ul>
                  </li>
				<?php
					}
				?>
								<?php
					if($_SESSION['Area']=="Secretaria"){
				?>
				<li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-book"></i>
                          <span>Orden de Compra</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="general.html">Nueva</a></li>
                          <li><a  href="buttons.html">Todas las ordenes</a></li>
                      </ul>
				  </li>

				  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-list"></i>
                          <span>Listado</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="blank.html">Corte</a></li>
                          <li><a  href="login.html">Sublimado</a></li>
						  <li><a  href="lock_screen.html">Costura</a></li>
						  <li><a  href="lock_screen.html">Empaquetado</a></li>
						  <li><a  href="lock_screen.html">Planchado</a></li>
						  <li><a  href="lock_screen.html">Compra</a></li>
						  <li><a  href="lock_screen.html">Finalizado</a></li>
                      </ul>
                  </li>
				<?php
					}
				?>
      
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      
      