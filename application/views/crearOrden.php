<section id="main-content">
    <section class="wrapper"> 
		<div class="row mt">
			<div class="col-lg-12">
				<div class="form-panel">
					<h4 class="mb"><i class="fa fa-angle-right"></i>Crear orden de compra</h4>
					<form action="" method="post" class="form-horizontal style-form" enctype="multipart/form-data">
						<div class="form-group">
							<div class="col-sm-2">
								<label class="control-label">Folio :</label>
								<input type="text" class="form-control" name="folio">
							</div>
							<div class="col-sm-2">
								<label class="control-label">Rut Cliente :</label>
								<input type="search" name="rut" list="ListaRut" id="Rut"oninput="obtenerID()" required>  
							<datalist id="ListaRut"> 
								<?php 
									foreach($clientes as $cliente){		
								?>	
									<option   data-IDCliente="<?= $cliente -> ID_Cliente?>"  value="<?= $cliente -> Rut?>" > </option> 
										<?php
											}
										?>
							</datalist>
							</select>
							</div>
							<div class="col-sm-2" style="visibility: hidden">
								<label class="control-label">ID_Cliente :</label>
								<input type="hidden" id="IDCliente" class="form-control" name="idCliente" readonly>
							</div>
							<div class="col-sm-2">
								<label class="control-label">Fecha Ingreso :</label>
								<input type="date" name="fechaIngreso" id="FechaIngreso" class="form-control round-form" readonly value="<?php echo date("Y-m-d");?>">
							</div>
							<div class="col-sm-2">
								<label class="control-label">Fecha Entrega :</label>
								<input type="date" name="fechaEntrega" id="FechaEntrega" class="form-control round-form" oninput="validate()" required>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-2">
								<label class="control-label">Tipo Trabajo :</label>
								<select type="text" id="TipoTrabajo" name="tipoTrabajo" class="form-control" placeholder="EstadoPedido" required>
									<option value="">Elige una opción</option>
									<option scope="">Arreglo</option>
									<option scope="">Fabricacion</option>
									<option scope="">Falla</option>
								</select>
							</div>
							<div class="col-sm-2">
								<label class="control-label">Pedido MercadoLibre :</label>
								<select type="text" id="PedidoExpress" name="pedidoExpress" class="form-control" placeholder="PedidoExpress"required >
									<option value="1">Si</option>
									<option value="0">No</option>
								</select>
							</div>
							<div class="col-sm-2">
							</div>
							<div class="col-sm-3">
								<label class="control-label">Ingresado Por</label>
								<input type="text" class="form-control" name="ingresadoPor" readonly value="<?php echo strtoupper( $_SESSION['Nombre']." ".$_SESSION['Apellido']);?>">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-2">
								<label class="control-label">Estado Pedido :</label>
								<select type="EstadoPedido" id="txtEstadoPedido" name="estadoPedido" class="form-control" placeholder="EstadoPedido">
									<option value="En espera del cliente">En espera del cliente</option>
									<option value="Confirmado">Confirmado</option></option>
									<option value="Corte">Corte</option>
									<option value="Sublimación">Sublimación</option>
									<option value="Corte">Corte</option>
									<option value="Estampado">Estampado</option>
									<option value="planchado">planchado</option>
									<option value="empaquetado">empaquetado</option>
									<option value="Compra">Compra</option>
									<option value="Espera de retiro">Espera de retiro</option>
									<option value="Despacho">Despacho</option>
									<option value="Finalizado">Finalizado</option>
								</select>
							</div>
						</div>	
						<input type="submit" name="">
					</form>
				</div>
			</div>
		</div>	
	</section>
</section> 

<script>


function validate(fechaEntrega) {
		// Despejar Puntos
	var inicio = document.getElementById('FechaIngreso').value;
	var fin = document.getElementById('FechaEntrega').value;
	if(fin<inicio){
		FechaEntrega.setCustomValidity("fecha menor a la de ingreso"); return false;
	}

 }	

  function  obtenerID() {
	// obtenermos el id que esta guardado en data-idcliente
	var rutCliente = $('#Rut').val();
	var ID_Cliente = $('#ListaRut [value="'+rutCliente+'"]').data('idcliente');
	// en otro imnput guardamos el ID
	$("#IDCliente").val(ID_Cliente);
 }	


</script>
