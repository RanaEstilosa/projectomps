<section id="main-content">
    <section class="wrapper"> 
		<div class="row mt">
			<div class="col-lg-12">
				<div class="form-panel">
					<h4 class="mb"><i class="fa fa-angle-right"></i>Producto</h4>
					<form action="" method="post" class="form-horizontal style-form" enctype="multipart/form-data">
						<div class="form-group">
							<div class="col-sm-2">
								<label class="control-label">Producto :</label>
								<select class="form-control" id="Producto" name="producto" > 
										<option  Value="Seleecione una opcion" > Seleecione una opcion </option>				
										<option  Value="Camisetas"> Camisetas </option>		
										<option  Value="Camisetas basquetbol"> Camisetas basquetbol </option>
										<option  Value="Camisetas muestra"> Camisetas muestra </option>			 
										<option  Value="Polerones"> Polerones </option>	 
										<option  Value="Shorts"> Shorts </option>		 
										<option  Value="Shorts basquetbol"> Shorts basquetbol </option>			 
										<option  Value="Shorts vivo"> Shorts vivo </option>		 
										<option  Value="Pantalones"> Pantalones </option>		 
										<option  Value="Chaquetas"> Chaquetas </option>
										<option  Value="Bolsos"> Bolsos </option>	 
										<option  Value="Calzas"> Calzas </option>
										<option  Value="Camisetas de Futbol Americano"> Camisetas de Futbol Americano </option>				 
										<option  Value="Shorts huincha"> Shorts huincha </option>
								</select>
							</div>
							<div class="col-sm-2">
								<label class="control-label">Genero :</label>
								<select class="form-control" id="Genero" name="genero" > 
								<option value="0">Seleccione una opcion</option>
								<option  Value="Hombre" > Hombre </option>
								<option  Value="Mujer" > Mujer </option>		
								<option  Value="Juvenil" > Juvenil </option>		
								<option  Value="Infantil" > Infantil </option>				
								</select>
							</div>
							<div class="col-sm-2">
								<label class="control-label">Talla :</label>
								<select class="form-control" id="Talla" name="talla" > 
								<option value="0">Seleccione una opcion</option>
								<?php
									foreach($tallas as $talla){
									?>
									<option value="<?= $talla->Talla?>"><?= $talla->Talla?> </option>
									<?php
									}
									?>
								</select>
								</select>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-2">
								<label class="control-label">Tipo Manga :</label>
								<select class="form-control" id="TipoManga" name="tipoManga" > 
								<option value="0">Seleccione una opcion</option>
									<?php
									foreach($tipomangas as $tipomanga){
									?>
									<option value="<?= $tipomanga->TipoManga?>"><?= $tipomanga->TipoManga?> </option>
									<?php
									}
									?>
								</select>
							</div>
							<div class="col-sm-2">
								<label class="control-label">Tipo Cuello :</label>
								<select class="form-control" id="TipoCuello" name="tipoCuello" > 
								<option value="0">Seleccione una opcion</option>
									<?php
									foreach($tipocuellos as $tipocuello){
									?>
									<option value="<?= $tipocuello->TipoCuello?>"><?= $tipocuello->TipoCuello?> </option>
									<?php
									}
									?>
								</select>
							</div>
							<div class="col-sm-2">
								<label class="control-label">ID Producto :</label>
								<input id="IDProducto" class="form-control" name="idProducto" value="Se llena con la consulta">
							</div>
						</div>			
						<input type="submit" name="">
					</form>
				</div>
			</div>
		</div>	
	</section>
</section> 

<script>
	$(document).ready(function(){
		$("#Genero").change(function () {
			$("#Genero option:selected").each(function () {
				obtenertallas=$('#Genero').val();
				$.post("obtenertalla", { obtenertallas: obtenertallas}, function(data){
				$("#Talla").html(data);
				}); 
			});
		}) 
	});
</script>
