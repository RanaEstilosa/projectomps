<section id="main-content">
    <section class="wrapper"> 
		<div class="row mt">
			<div class="col-lg-12">
				<div class="form-panel">
					<h4 class="mb"><i class="fa fa-angle-right"></i>Producto</h4>
					<form action="" method="post" class="form-horizontal style-form" enctype="multipart/form-data">
						<div class="form-group" id="formulario">
							<div class="col-sm-2">
								<label class="control-label">Producto :</label>
								<select class="form-control" id="Producto" name="producto" oninput="obtenerIDProducto()" > 
								<option value="0">Seleccione una opcion</option>
									<?php
									foreach ($nombreproductos as $nombreproducto){ 
									?>
										<option data-IDProducto="<?= $nombreproducto->ID_TipoProducto?>" value="<?= $nombreproducto->Nombre?>" ><?= $nombreproducto->Nombre?> </option>
									<?php
									}
									?>
								</select>
							</div>
							<div class="col-sm-2" style="visibility: hidden">
								<label class="control-label">ID_TipoProducto :</label>
								<input type="" id="IDTipoProducto" class="form-control" name="idtipoproducto" readonly>
							</div>
							<div class="col-sm-2" id=DivGenero>
								<label class="control-label">Genero :</label>
								<select class="form-control" id="Genero" name="genero" > 
								<option value="0">Seleccione una opcion</option>
								<?php
									foreach($generos as $genero){
									?>
									<option value="<?= $genero->Genero?>"><?= $genero->Genero?> </option>
									<?php
									}
									?>
								</select>
							</div>
							<div class="col-sm-2">
								<label class="control-label">Talla :</label>
								<select class="form-control" id="Talla" name="talla" > 
								<option value="0">Seleccione una opcion</option>
									<?php
									foreach($tallas as $talla){
									?>
									<option value="<?= $talla->Talla?>"><?= $talla->Talla?> </option>
									<?php
									}
									?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-2">
								<label class="control-label">Tipo Manga :</label>
								<select class="form-control" id="TipoManga" name="tipomanga"> 
								<option value="0">Seleccione una opcion</option>
								<?php
									foreach($tipomangas as $tipomanga){
									?>
									<option value="<?= $tipomanga->TipoManga?>"><?= $tipomanga->TipoManga?> </option>
									<?php
									}
									?>
								</select>
							</div>
							<div class="col-sm-2">
								<label class="control-label">Tipo Cuello :</label>
								<select class="form-control" id="TipoCuello" name="tipocuello"> 
								<option value="0">Seleccione una opcion</option>
								<?php
									foreach($tipocuellos as $tipocuello){
									?>
									<option value="<?= $tipocuello->TipoCuello?>"><?= $tipocuello->TipoCuello?> </option>
									<?php
									}
									?>
								</select>
							</div>
							<div class="col-sm-2">
								<label class="control-label">ID Producto :</label>
								<select class="form-control" id="IDProducto" name="idproducto" readonly> 
									<?php
									foreach($idproductos as $idproducto){
									?>
									<option value="<?= $idproducto->ID_Producto?>"><?= $idproducto->ID_Producto?> </option>
									<?php
									}
									?>
								</select>
							</div>
						</div>			
						<input type="submit" name="">
					</form>
				</div>
			</div>
		</div>	
	</section>
</section> 

<script>

function  obtenerIDProducto() {
	// obtenermos el id que esta guardado en data-IDTipoProducto
	var rutCliente = $('#Producto').val()
	var ID_Cliente = $('#Producto [value="'+rutCliente+'"]').data('idproducto');
	// en otro imnput guardamos el ID
	$("#IDTipoProducto").val(ID_Cliente);
 }	


	$(document).ready(function(){
		$('#Producto').change(function(){
			CargarGeneros();
			CargarTallas();
			CargaTipoMangas();
			CargaTipoCuellos();
			CargaIDProducto();
		});
		$('#Genero').change(function(){
			CargarTallas();
			CargaTipoMangas();
			CargaTipoCuellos();
			CargaIDProducto();
		});
		$('#Talla').change(function(){
			CargaTipoMangas();
			CargaTipoCuellos();
			CargaIDProducto();
		});
		$('#TipoManga').change(function(){
			CargaTipoCuellos();
			CargaIDProducto();
		});
		$('#TipoCuello').change(function(){
			CargaIDProducto();
		});
    })
</script>	

<script>
	function CargarGeneros(){
		$.ajax({
			type:"POST",
			url:"ObtenerProductoGenero",
			data:"producto=" + $('#Producto').val(),
			success:function(r){
				$('#Genero').html(r);
			}
		});
	}

	function CargarTallas(){
		$.ajax({
			type:"POST",
			url:"ObtenerProductoTalla",
			data:"genero=" + $('#Genero').val(),
			success:function(r){
				$('#Talla').html(r);
			}
		});
	}
	function CargaTipoMangas(){
		$.ajax({
			type:"POST",
			url:"ObtenerProductoTipoManga",
			data:"talla=" + $('#Talla').val(),
			success:function(r){
				$('#TipoManga').html(r);
			}
		});
	}
	function CargaTipoCuellos(){
		$.ajax({
			type:"POST",
			url:"ObtenerProductoTipoCuello",
			data:"tipomanga=" + $('#TipoManga').val(),
			success:function(r){
				$('#TipoCuello').html(r);
			}
		});
	}	
	function CargaIDProducto(){
		$.ajax({
			type:"POST",
			url:"ObtenerIDProducto",
			data:{tipocuello : $('#TipoCuello').val() ,
				  tipomanga :  $('#TipoManga').val() ,
				  talla :  $('#Talla').val() ,
				  genero :  $('#Genero').val() ,
				  producto :  $('#Producto').val() ,
				  },
			success:function(r){
				$('#IDProducto').html(r);
			}
		});
	}

</script>
