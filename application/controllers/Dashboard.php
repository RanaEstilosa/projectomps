<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {


	public function index(){
		$this->loadView("home");
	}

	public function login() {
		if($_POST['username'] && $_POST['password']){
			$login=$this->Site_model->loginUser($_POST);
			if($login){
				$array=array(
					"ID_Trabajador" => $login[0]->ID_Trabajador,
					"Nombre" => $login[0]->Nombre,
					"Apellido" => $login[0]->Apellido,
					"Area" => $login[0]->Area,
					"Username" => $login[0]->Username,
					"Password" => $login[0]->Password,
				);

				if($login[0]->Area=="Externo"){
					$array['tipo']="Externo";
				
				}else if($login[0]->Area=="Cortador"){
					$array['tipo']="Cortador";
				
				}else if($login[0]->Area=="Taller"){
					$array['tipo']="Taller";
				
				}else if($login[0]->Area=="Admin"){
					$array['tipo']="Admin";
				
				}else if($login[0]->Area=="Secretaria"){
					$array['tipo']="Secretaria";
				
				}
				$this->session->set_userdata($array);
			//	print_r($_SESSION);
			}
		}
		$this->loadView('login');
	}

	public function logout(){
		if($_SESSION['Username']){
			$this->session->sess_destroy();
			redirect(base_url()."Dashboard/login","location");
		}
		
	}

	function loadView($view,$data=NULL){
		// SI TENEMOS UNA SESION CREADA 
		//	print_r($_SESSION);
		if($_SESSION['Username']){
			// si la vista es login se redirige a home
			if($view=="login"){
				redirect(base_url()."Dashboard","location");
			}
			// si es una vista cualquiea se carga
			$this->load->view('includes/header');
			$this->load->view('includes/sidebar');
			$this->load->view($view,$data);
			$this->load->view('includes/footer');
			// si no tenemos iniciada sesion
		}else{
			// si la vista es login se carga
			if($view=="login"){
				$this->load->view($view);
			}else{
				// si la vista es otra cualquiera se redirige a login
				redirect(base_url()."Dashboard/login","location");
			}
		}
	}


	function gestionTrabajadores(){

		if($_SESSION['Area']=="Admin"){
			$data['trabajadores']=$this->Site_model->getTrabajadores();
			$this->loadView("gestionTrabajadores",$data);
		}else{
			redirect(base_url()."Dashboard","location");
		}

	}

	function eliminarTrabajador(){
		if($_POST['IDTrabajador']){
			$this->Site_model->deleteTrabajador($_POST['IDTrabajador']);
		}
	}

	function gestionProductos(){
		if($_SESSION['Area']=="Admin"){
			$data['productos']=$this->Site_model->getProductos();
			$this->loadView("gestionProductos",$data);
			if($_POST) {
				$this->Site_model->uploadProducto($_POST);
			}	
		}else{
			redirect(base_url()."Dashboard","location");
		}


	}

	function eliminarProducto(){
		if($_POST['IDProducto']){
			$this->Site_model->deleteProducto($_POST['IDProducto']);
		}
	}

	/* function gestionColores(){

		if($_SESSION['Area']=="Admin"){
			$data['colores']=$this->Site_model->getColor();
			$this->loadView("gestionColores",$data);
		}else{
			redirect(base_url()."Dashboard","location");
		}
	}*/

	function gestionColores(){

		if($_SESSION['Area']=="Admin"){
			$data['colores']=$this->Site_model->getColor();
			$this->loadView("gestionColores",$data);
			if($_POST) {
				$this->Site_model->uploadColor($_POST);
			}	
		}else{
			redirect(base_url()."Dashboard","location");
		}
	}

	function eliminarColor(){
		if($_POST['IDColor']){
			$this->Site_model->deleteColor($_POST['IDColor']);
		}
	}

	function gestionEstiloEquipos(){
		if($_SESSION['Area']=="Admin"){
			$data['estiloequipos']=$this->Site_model->getEstiloEquipo();
			$this->loadView("gestionEstiloEquipos",$data);
			if($_POST) {
				$this->Site_model->uploadEstiloEquipo($_POST);
			}	
		}else{
			redirect(base_url()."Dashboard","location");
		}
	}

	function eliminarEstiloEquipo(){

		if($_POST['IDEstiloEquipo']){
			$this->Site_model->deleteEstiloEquipo($_POST['IDEstiloEquipo']);
		}

	}

	function crearOrden() {
		if($_POST) {
			if($_FILES['archivo']) {

				$config['upload_path']          = './uploads/';
				$config['allowed_types']        = 'gif|jpg|png';
				//$config['max_size']             = 100;
				//$config['max_width']            = 1024;
				//$config['max_height']           = 768;
				$config['file_name']=uniqid().$_FILES['archivo']['name'];
					
				 $this->load->library('upload', $config); 
				if ( ! $this->upload->do_upload('archivo')) {
					echo "Error";
					
				//	$error = array('error' => $this->upload->display_errors());
				//	$this->load->view('upload_form', $error);
				
				}else{
					
					$this->Site_model->uploadOrden($_POST,$config['file_name']);
				//	$data = array('upload_data' => $this->upload->data());
				//		$this->load->view('upload_success', $data);
				}

			}else{
				$this->Site_model->uploadOrden($_POST);
			}	
		}
		
	/*	function ordenProductoGenero() {
			if($_POST) {
				$this->Site_model->uploadOrdenProductogenero($_POST);
			 }
			
	
			$data['nombreproductos']=$this->Site_model->getOrdenProductoNombre(); 
			$this->loadView("productoGenero",$data);
			
		}*/

		$data['clientes']=$this->Site_model->getCliente();
		$this->loadView("crearOrden",$data);
	}

	function ordenProducto() {
		if($_POST) {
			$this->Site_model->uploadOrdenProducto($_POST);
		 }
		
		$data['generos']=$this->Site_model->getOrdenProductoGenero(); 
		$data['tipomangas']=$this->Site_model->getOrdenProductoTipoManga(); 
		$data['tipocuellos']=$this->Site_model->getOrdenProductoTipoCuello(); 
		
		$this->loadView("crearProducto",$data);
	}

	function ordenProductoGenero() {
		if($_POST) {
			$this->Site_model->uploadOrdenProductogenero($_POST);
		 }
		

		$data['nombreproductos']=$this->Site_model->getOrdenProductoNombre(); 
		$this->loadView("productoGenero",$data);
		
	}

	function ObtenerProductoGenero() {
		$data['generos']=$this->Site_model->getOrdenProductoGenero(); 
		$this->load->View("GetOrdenProductoGenero",$data);	
	}

	function ObtenerProductoTalla() {
		$data['tallas']=$this->Site_model->getOrdenProductoTalla(); 
		$this->load->View("GetOrdenProductoTalla",$data);	
	}
	function ObtenerTalla() {
		$data['tallas']=$this->Site_model->getOrdenProductoTalla(); 
		$this->load->View("GetOrdenProductoTalla",$data);	
	}

	function ObtenerProductoTipoManga() {
		$data['tipomangas']=$this->Site_model->getOrdenProductoTipoManga(); 
		$this->load->View("GetOrdenProductoTipoManga",$data);	
	}

	function ObtenerProductoTipoCuello (){
		$data['tipocuellos']=$this->Site_model->getOrdenProductoTipoCuello(); 
		$this->load->View("GetOrdenProductoTipoCuello",$data);	
	}

	

	function ObtenerIDProducto (){
		$data['idproductos']=$this->Site_model->getOrdenIDProducto(); 
		$this->load->View("GetOrdenIDProducto",$data);	
	}



	function cliente(){

		if($_SESSION['Area']=="Admin" || $_SESSION['Area']=="Admin" ){
			$data['clientes']=$this->Site_model->getCliente();
			$this->loadView("crearCliente",$data);
			if($_POST) {
				$this->Site_model->uploadCliente($_POST);
			}	
		}else{
			redirect(base_url()."Dashboard","location");
		}
	}

	function misOrden(){
		if ($_SESSION['ID_Trabajador']){
			$data['ordenes']=$this->Site_model->getOrdenes($_SESSION['Area']);
			$this->loadView("misOrden",$data);
		}else {
			redirect(base_url()."Dashboard","location");
		}
		
	}

	function CrearOrdenCompra () {
		$data['clientes']=$this->Site_model->getCliente();
		$data['productos']=$this->Site_model->getProductos();
		$data['colores']=$this->Site_model->getColor();
		$data['series']=$this->Site_model->getSerie();
		$data['estiloequipos']=$this->Site_model->getEstiloEquipo();
		$this->loadview("CrearOrdenCompra",$data);
		if($_POST) {
			$this->Site_model->uploadDetalleProducto($_POST);
		 }
	}

	function GuardarPedido(){
		$this->Site_model->uploadOrden();
	}
	
	function detalleproducto(){
		$this->loadview("detalleproducto");
		if($_POST) {
			$this->Site_model->insert($_POST);
		 }
	}

	function Uploaddetalleproducto(){
		$this->Site_model->uploadDetalleProducto($_POST);
	}

	function InsertarPedido1(){
		$this->Site_model->uploadProducto1($_POST);
	}

}
