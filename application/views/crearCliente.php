<section id="main-content">
    <section class="wrapper"> 
		<div class="row mt">
			<div class="col-lg-12">
				<div class="form-panel">
					<h4 class="mb"><i class="fa fa-angle-right"></i>Formulario Inscripcion Cliente</h4>
					<form action="" method="post" class="form-horizontal style-form" data-toggle="validator" role="form">
					<div class="form-group">
						<label class="col-sm-2 col-sm-2 control-label" for="inputNombre" >Nombre</label>
						<div class="col-sm-10">
						<input type="text" class="form-control" id="inputNombre" name="nombre" required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 col-sm-2 control-label" for="inputApellido" >Apellido</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="inputApellido" name="apellido" required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 col-sm-2 control-label" for="inputRut">Rut</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="inputRut" name="rut"  oninput="checkRut(this)" required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 col-sm-2 control-label" for="inputTelefono" >Telefono</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="inputTelefono" name="telefono" required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 col-sm-2 control-label" for="inputEmail" >Email</label>
						<div class="col-sm-10">
							<input type="email" class="form-control" id="inputEmail" name="email" required>
						</div>
					</div>
					<button type="SUBMIT" class="btn btn-info">Enviar</button>
					</form>
				</div>
				<table id="ListaClientes" class="display">
			<thead>
				<tr>
					<th>ID</th>
					<th>Nombre</th>
					<th>Apellido</th>
					<th>Rut</th>
					<th>Telefono</th>
					<th>Correo</th>
				</tr>
			</thead>
			<tbody>

				<?php
				foreach ($clientes as $cliente) {
				
				?>
				<tr id="rowCliente<?= $cliente->ID_Cliente?>">
					<td> <?= $cliente->ID_Cliente?> </td>
					<td> <?= $cliente->Nombre?> </td>
					<td> <?= $cliente->Apellido?> </td>
					<td> <?= $cliente->Rut?> </td>
					<td> <?= $cliente->Telefono?> </td>
					<td> <?= $cliente->Correo?> </td>
				</tr>

				<?php
				}
				?>
			</tbody>
		</table>
			</div>
		</div>	
	</section>
</section> 




<script>
	$(document).ready( function () {
    	$('#ListaClientes').DataTable();
} );
</script>


<script>
	function checkRut(rut) {
		// Despejar Puntos
		var valor = rut.value.replace('.','');
		// Despejar Guion
		valor = valor.replace('-','');
		
		// Aislar Cuerpo y Di­gito Verificador
		cuerpo = valor.slice(0,-1);
		dv = valor.slice(-1).toUpperCase();
		
		// Formatear RUN
		rut.value = cuerpo + '-'+ dv
		
		// Si no cumple con el minimo ej. (n.nnn.nnn)
		if(cuerpo.length < 7) { rut.setCustomValidity("RUT Incompleto"); return false;}
		
		// Calcular Di­gito Verificador
		suma = 0;
		multiplo = 2;
		
		// Para cada di­gito del Cuerpo
		for(i=1;i<=cuerpo.length;i++) {
		
			// Obtener su Producto con el Miºltiplo Correspondiente
			index = multiplo * valor.charAt(cuerpo.length - i);
			
			// Sumar al Contador General
			suma = suma + index;
			
			// Consolidar Miºltiplo dentro del rango [2,7]
			if(multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }
	
		}
		
		// Calcular Di­gito Verificador en base al Mi³dulo 11
		dvEsperado = 11 - (suma % 11);
		
		// Casos Especiales (0 y K)
		dv = (dv == 'K')?10:dv;
		dv = (dv == 0)?11:dv;
		
		// Validar que el Cuerpo coincide con su DÃ­gito Verificador
		if(dvEsperado != dv) { rut.setCustomValidity("RUT Inválido"); return false; }
		
		// Si todo sale bien, eliminar errores (decretar que es válido)
		rut.setCustomValidity('');
	}
</script>
